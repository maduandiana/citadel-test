export const DELETE_ADDRESS = 'DELETE_ADDRESS';
export const EDIT_ADDRESS = 'EDIT_ADDRESS';
export const GET_BALANCE = 'GET_BALANCE';
export const SAVE_BALANCE = 'SAVE_BALANCE';
export const SET_LOADING = 'SET_LOADING';
export const SAVE_NEW_BALANCE = 'SAVE_NEW_BALANCE';
export const SET_ERROR = 'SET_ERROR';
export const CLEAR_DATA = 'CLEAR_DATA';
export const UPDATE_BALANCE = 'UPDATE_BALANCE';

