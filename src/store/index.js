import Vue from 'vue'
import Vuex from 'vuex'
import balanceModule from './modules/balanceModule';
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
   balanceModule
  }
});

export default store;