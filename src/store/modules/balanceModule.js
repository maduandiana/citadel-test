import axios from 'axios'
import {SAVE_BALANCE,CLEAR_DATA,DELETE_ADDRESS,EDIT_ADDRESS,SET_LOADING,SAVE_NEW_BALANCE,SET_ERROR,UPDATE_BALANCE} from '../mutation-types'

const balanceModule = {
    state: {
        balance: localStorage.getItem('balances') ? JSON.parse(localStorage.getItem('balances')) : [],
        loading:false,
        converted:1,
        error:false,
    },
    watch: {
      balance: {
          handler: function(updatedList) {
            localStorage.setItem('balances', JSON.stringify(updatedList));
          },
          deep: true
      }
    },
    actions:{
      getBalance({ commit },address) {
        axios.get('https://api.blockchain.info/haskoin-store/btc/address/'+ address + '/balance')
        .then(response => {
        commit('SET_LOADING', false)
        commit('SAVE_BALANCE', response.data)
      }).catch(error => console.log(error));
      },

      updateBalance({ commit },balanceArray) {
        commit('CLEAR_DATA')
        balanceArray.map(item => {
          axios.get('https://api.blockchain.info/haskoin-store/btc/address/'+ item.address + '/balance')
          .then(response => {
          commit('SET_LOADING', false)
          commit('UPDATE_BALANCE', response.data)
        }).catch(error => console.log(error));
        })
      },

      convertBalance({ commit }) {
        axios.get('https://free.currconv.com/api/v7/convert?q=BTC_EUR&compact=ultra&apiKey=0d0b4165628e9cc241a9')
        .then(response => {
        commit('SAVE_NEW_BALANCE', response.data)
      }).catch(error => {
        console.log(error);
        commit('SET_ERROR', true)
      });
      }
    },
    getters:{
      getBalance(state) {
        return state.balance 
      },
    },
    mutations: {
      [UPDATE_BALANCE](state, response) {
        state.balance.push({
            address: response.address,btc:(response.received/100000000),id:state.balance.length + 1
          });
        localStorage.setItem('balances', JSON.stringify(state.balance));
      },
      [SAVE_BALANCE](state, response) {
        state.balance.push({
            address: response.address,btc:(response.received/100000000),id:state.balance.length + 1
          });
        localStorage.setItem('balances', JSON.stringify(state.balance));
        window.location.href = '/'
      },
      [SAVE_NEW_BALANCE](state, response) {
        state.converted = response.BTC_EUR
      },
      [CLEAR_DATA](state) {
        state.balance = []
      },
      [EDIT_ADDRESS] (state,data) {
        state.balance.map((item,i) =>{
          if(item.id == data.id){
            state.balance.splice(i, 1)
            localStorage.setItem('balances', JSON.stringify(state.balance));
          }
        })
        this.dispatch("getBalance",data.value); 
      },
      [SET_LOADING] (state,val) {
        state.loading = val
      },
      [SET_ERROR] (state,val) {
        state.error = val
      },
      [DELETE_ADDRESS] (state,data) {
        state.balance.map((item,i) =>{
          if(item.id == data.id && item.address == data.address){
            state.balance.splice(i, 1)
          }
        })
        localStorage.setItem('balances', JSON.stringify(state.balance));
        window.location.href = '/'
      }
    }
  }

  export default balanceModule